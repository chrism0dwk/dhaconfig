import setuptools

setuptools.setup(
    name='dhaconfig',
    version='0.0.1',
    author='Alison Hale',
    author_email='a.c.hale@lancaster.ac.uk',
    description='Dynamic Health Atlas configuration',
    url='https://gitlab.com/achale/dhaconfig',
    packages=setuptools.find_packages(),
    install_requires=['datetime','geojson','geopandas'],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.7',
)

