# Dynamic Health Aatlas configuration with Python

Helper functions for configuring the Dynamic Atlas using Python it would be advisable to follow the tutorial (details below) and build a test website before attempting to use these functions.

Use Python to create the configuration and geojson data for the Dynamic Health Atlas.
There are two classes in the module `config.py`: 
- `utilities` contains functions to read and write data
- `dha` is used to build the config data

Example of basic usage is given in `examples.py`.


### Background
The Dynamic Atlas software is a web-based application designed to display geospatial data which changes over time.  A working demo is available at https://achale.gitlab.io/dynamicatlas/. A tutorial which can be accessed at https://achale.gitlab.io/dynamicatlastutorial/ and downloaded from https://gitlab.com/achale/dynamicatlastutorial/.  The Dynamic Atlas is a stand-alone application however if you would prefer to run it within a Shiny framework then see https://gitlab.com/achale/dynamicatlasshiny for further details.
